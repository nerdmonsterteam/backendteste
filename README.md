# NerdMonster Digital Retail - Programador PHP Backend

## Instruções para publicação

1. Para publicar seu projeto você deverá fazer um fork desse repositório.
2. Você pode usar o framework Laravel ou fazer apenas com PHP puro.
3. No caso do uso de Laravel, deve ser utilizado os migrations para criação da tabela.
4. Você pode usar a versão 7.1 >= do PHP.
5. Você pode usar a versão 5.5 >= do MySQL.
6. Você deve fazer um Pull Request para esse repositório para entregar o teste.

## Objetivo

1. Criar um Crud, onde será possível Criar/Editar/Excluir/Listar usuários.
2. O sistema também deve possuir a possibilidade de vincular/desvincular várias cores ao usuário.

## Estrutura de banco de dados 

A seguinte estrutura será utilizada para persistência dos dados, podendo ser alterada a qualquer momento para melhor funcionamento do sistema:

1. tabela: users, campos: id, name, email
2. tabela: colours, campos: id, name
3. tabela: users_colours, campos: id, colour_id, user_id

## Boa sorte
Use seu conhecimento, consulte a documentação e o google, caso ainda houver dúvidas, nos pergunte :D.

## Pontos que serão levados em conta:

1. Funcionalidade
2. Organização